using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void niveles()
    {
        // Cargar la escena de seleccion de niveles
        SceneManager.LoadScene(1); // Deberia enviar al usuario al selector de niveles
    }

    public void volverMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void salir()
    {
        // Salir del juego (solo funciona en builds, no en el editor de Unity)
        Application.Quit();
    }
}
