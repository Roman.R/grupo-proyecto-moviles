using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 3;  // Vida m�xima del enemigo
    public GameObject explosionPrefab;
    public Transform explosionPoint;
    private int currentHealth;  // Vida actual del enemigo
    public float speed = 2f;  // Velocidad de movimiento del enemigo
    private Transform player;  // Referencia al transform del jugador

    void Start()
    {
        // Inicializar la vida del enemigo
        currentHealth = maxHealth;

        // Encontrar el objeto del jugador
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        // Mover hacia el jugador
        MoveTowardsPlayer();
    }

    void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto que colision� es un proyectil
        if (collision.gameObject.CompareTag("Projectile"))
        {
            // Reducir la vida del enemigo
            TakeDamage(1);

            // Destruir el proyectil
            Destroy(collision.gameObject);
        }
        
        
            
        
    }

    void TakeDamage(int damage)
    {
        // Reducir la vida del enemigo
        currentHealth -= damage;

        // Verificar si la vida del enemigo ha llegado a 0
        if (currentHealth <= 0)
        {
            // Destruir el enemigo
            Destroy(gameObject);

            Explosion();


        }
    }

    public void Explosion()
    {
        Instantiate(explosionPrefab, explosionPoint.position, explosionPoint.rotation);
    }

    void MoveTowardsPlayer()
    {
        // Verificar que el jugador existe
        if (player != null)
        {
            // Direccionar hacia el jugador
            Vector3 direction = (player.position - transform.position).normalized;
            // Mover en la direcci�n del jugador
            transform.position += direction * speed * Time.deltaTime;
        }
    }
}
