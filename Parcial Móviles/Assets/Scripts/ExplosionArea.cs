using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionArea : MonoBehaviour
{
    public float speed = 10f;  // Velocidad del proyectil
    public float lifetime = 5f;
    private Rigidbody rb;      // Referencia al componente Rigidbody

    void Start()
    {
        // Obtener el componente Rigidbody
        rb = GetComponent<Rigidbody>();

        // Mover el proyectil hacia adelante en el eje Z
        rb.velocity = transform.forward * speed;
        

        Destroy(gameObject, lifetime);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Destruir el proyectil al colisionar con algo
        Destroy(gameObject);
    }
}
