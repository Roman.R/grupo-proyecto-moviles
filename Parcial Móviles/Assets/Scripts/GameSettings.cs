using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSettings : MonoBehaviour
{
    //Textos
    public TMPro.TMP_Text textoVida;
    public TMPro.TMP_Text textoPuntaje;
    public TMPro.TMP_Text textoPierde;

    //Contadores
    //Puntaje
    private int contSumaPuntos;
    public int recompensaPuntos = 0;

    //Vida
    public int contVida = 3;

    void Start()
    {
        Time.timeScale = 1;
        textoPierde.text = "";
    }

    void Update()
    {
        textoVida.text = "" + contVida.ToString("f0");
        //Contador de vida
        if (contVida <= 0)
        {
            textoPierde.text = "Moriste rey. R para Reintentar";
            Time.timeScale = 0;
        }

        resetEscenaTecla();

    }

    private void entregaPuntos()
    {
        //textoPuntaje.text = "Puntaje: " + contSumaPuntos.ToString();
    }

    private void resetEscenaTecla()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
    }

    public void resetEscenaMovil()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") == true)
        {
            contVida = contVida - 1;
            entregaPuntos();
            other.gameObject.SetActive(false);
        }

    }
}