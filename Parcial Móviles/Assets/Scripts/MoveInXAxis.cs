using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInXAxis : MonoBehaviour
{
    private Rigidbody rb;

    public float speed = 5f;  // Velocidad de movimiento
    public GameObject projectilePrefab;  // Prefab del proyectil
    public Transform firePoint;  // Punto desde donde se disparan los proyectiles
    public float shootCooldown = 1f;  // Tiempo de espera entre disparos en segundos

    private float shootTimer;  // Temporizador para gestionar el cooldown

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Mover al personaje en el eje X
        float move = Input.GetAxis("Horizontal");
        Vector3 newPosition = transform.position + new Vector3(move * speed * Time.deltaTime, 0, 0);
        transform.position = new Vector3(newPosition.x, transform.position.y, transform.position.z);

        // Actualizar el temporizador del cooldown
        shootTimer += Time.deltaTime;

        // Disparar proyectil al presionar la barra espaciadora y si el cooldown ha pasado
        if (Input.GetKeyDown(KeyCode.Space) && shootTimer >= shootCooldown)
        {
            Shoot();
            shootTimer = 0f;  // Reiniciar el temporizador del cooldown
        }
    }

    public void moveUp()
    {
        rb.AddForce(new Vector3(1, 0, 0), ForceMode.Impulse);
    }
    public void moveDown()
    {
        rb.AddForce(new Vector3(-1, 0, 0), ForceMode.Impulse);
    }

    public void Shoot()
    {
        // Instanciar el proyectil en el punto de disparo
        Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        Debug.Log("Vibration On");
        Handheld.Vibrate();
    }
}
