using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionManager : MonoBehaviour
{
    public void Nivel1()
    {
        SceneManager.LoadScene(2); // Nivel 1
    }

    public void Nivel2()
    {
        SceneManager.LoadScene(3); //Nivel 2
    }

    public void volverMenu()
    {
        SceneManager.LoadScene(0); //Boton Volver al menu
    }


}
